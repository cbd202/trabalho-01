Construção de Banco de Dados - UFRJ - 2020.2

Esse repósitório tem a finalidade de disponibilizar os resultados do desenvolvimento do Projeto 1 da Disciplina "Construção de Banco de Dados", oferecida no curso de Engenharia de Computação e Informação pelo Professor Milton Ramos Ramirez durante o período letivo de 2020.2.

Alunas:
- Ana Carolina Jaolino
- Juliana Piaz
- Lidiana Souza
